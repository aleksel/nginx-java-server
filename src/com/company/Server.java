package com.company;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class Server implements Runnable {

    private int serverPort = 8080;
    private ServerSocket serverSocket = null;
    private boolean isStopped = false;
    private Thread runningThread = null;

    Server(int port) {
        this.serverPort = port;
    }

    public void run() {
        synchronized (this) {
            this.runningThread = Thread.currentThread();
        }
        openServerSocket();
        while (!isStopped()) {
            Socket clientSocket;
            try {
                clientSocket = this.serverSocket.accept();
            } catch (IOException e) {
                if (isStopped()) {
                    System.out.println("Server Stopped.");
                    return;
                }
                throw new RuntimeException("Error accepting client connection", e);
            }
            InputStream in;
            try {
                in = clientSocket.getInputStream();
                OutputStream out = clientSocket.getOutputStream();
                //System.out.println(Thread.currentThread().getName());
                //System.out.println(new Scanner(in, "UTF-8").useDelimiter("\\r\\n\\r\\n").next());
                Scanner scanner = new Scanner(in, "UTF-8").useDelimiter("\\r\\n\\r\\n");
                if (scanner.hasNext()) scanner.next();

                writeResponse("<html><body><h1>Hello from Habrahabr</h1></body></html>", out);
                clientSocket.close();
            } catch (IOException e) {
                System.out.println("Error reading data");
            }
        }
        System.out.println("Server Stopped.");
    }

    private void writeResponse(String s, OutputStream out) throws IOException {
        String response = "HTTP/1.1 200 OK\r\n" +
                "Server: YarServer/2009-09-09\r\n" +
                "Content-Type: text/html\r\n" +
                "Content-Length: " + s.length() + "\r\n" +
                "Connection: close\r\n\r\n";
        String result = response + s;
        out.write(result.getBytes());
        out.flush();
    }

    private synchronized boolean isStopped() {
        return this.isStopped;
    }

    public synchronized void stop() {
        this.isStopped = true;
        try {
            this.serverSocket.close();
        } catch (IOException e) {
            throw new RuntimeException("Error closing server", e);
        }
    }

    private void openServerSocket() {
        try {
            this.serverSocket = new ServerSocket(this.serverPort);
            System.out.println("Server Started on port: " + this.serverPort);
        } catch (IOException e) {
            throw new RuntimeException("Cannot open port: " + this.serverPort, e);
        }
    }
}