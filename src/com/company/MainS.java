package com.company;

import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

public class MainS {

    public static final Queue<Socket> queue = new ArrayBlockingQueue<Socket>(700000);

    public static void main(String[] args) throws Throwable {
        SocketProcessorS socketProcessor = new SocketProcessorS();
        ServerSocket ss = new ServerSocket(9000, 10000, InetAddress.getByName("0.0.0.0"));
        while (true) {
            Socket accept = ss.accept();
            //long start = System.nanoTime();
            socketProcessor.run(accept);
            //System.out.println(System.nanoTime() - start);
        }
    }
}
