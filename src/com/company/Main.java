package com.company;

import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Queue;
import java.util.concurrent.*;

public class Main {

    public static final Queue<Socket> queue = new ArrayBlockingQueue<Socket>(700000);

    public static void main(String[] args) throws Throwable {
        /*Server server = new Server(9000);
        new Thread(server).start();*/

        ExecutorService service = Executors.newFixedThreadPool(1);
        for (int i = 0; i < 1; i++) {
            service.submit(new SocketProcessor());
        }
        ServerSocket ss = new ServerSocket(9000, 1000, InetAddress.getByName("0.0.0.0"));
        while (true) {
            queue.add(ss.accept());
            //System.err.println("Client accepted");
            //new Thread(new SocketProcessor(s)).start();
        }

        /*try {
            Thread.sleep(20 * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Stopping Server");
        server.stop();*/
    }
}
