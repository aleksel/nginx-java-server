package com.company;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

public class SocketProcessorS {

    public void run(Socket s) {
        try {
            InputStream is = s.getInputStream();
            OutputStream os = s.getOutputStream();
            readInputHeaders(is);
            writeResponse("<html><body><h1>Hello from Habrahabr</h1></body></html>", os);
            //System.out.println(Thread.currentThread().getName());
        } catch (Throwable t) {
                /*do nothing*/
        } finally {
            try {
                s.close();
            } catch (Throwable t) {
                    /*do nothing*/
            }
        }
        //System.err.println("Client processing finished");
    }

    private void writeResponse(String s, OutputStream os) throws Throwable {
        String response = "HTTP/1.1 200 OK\r\n" +
                "Server: YarServer/2009-09-09\r\n" +
                "Content-Type: text/html\r\n" +
                "Content-Length: " + s.length() + "\r\n" +
                "Connection: close\r\n\r\n";
        String result = response + s;
        os.write(result.getBytes());
        os.flush();
    }

    private void readInputHeaders(InputStream is) throws Throwable {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        while (true) {
            String s = br.readLine();
            if (s == null || s.trim().length() == 0) {
                break;
            }
        }
    }
}
